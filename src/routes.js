import { Router } from 'express';
import RuleController from './app/controllers/RuleController';
import ScheduleController from './app/controllers/ ScheduleController';

const routes = new Router();

routes.get('/', (req, res) => {
  return res.json({ message: 'Api ok!' });
});

routes.get('/rules', RuleController.index);
routes.post('/rules', RuleController.store);
routes.delete('/rules/:id', RuleController.delete);

routes.post('/schedules', ScheduleController.index);

export default routes;
