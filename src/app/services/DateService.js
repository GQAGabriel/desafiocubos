class DateService {
  convertDateEn(d) {
    const [date, month, year] = d.split('-').map(n => parseInt(n, 10));
    const en_date = new Date(year, month - 1, date);
    return en_date;
  }

  convertDatePt(d) {
    const date_string = d.toISOString();
    const en_date = date_string.split('T')[0];
    const parts = en_date.split('-');
    const pt_date = `${parts[2]}-${parts[1]}-${parts[0]}`;
    return pt_date;
  }
}

export default new DateService();
