import fs from 'fs';
import * as Yup from 'yup';
import { isAfter, isEqual, addDays, getDay } from 'date-fns';
import { resolve } from 'path';
import DateService from '../services/DateService';

const filePath = resolve(__dirname, '..', 'rules.json');
const file = fs.readFileSync(filePath, 'utf-8');

class ScheduleController {
  async index(req, res) {
    const schema = Yup.object().shape({
      start_date: Yup.string().required(),
      end_date: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro de validação dos campos.' });
    }

    const { start_date, end_date } = req.body;
    const en_start_date = DateService.convertDateEn(start_date);
    const en_end_date = DateService.convertDateEn(end_date);
    const rules = JSON.parse(file);
    let date = en_start_date;
    let intervals = [];
    let schedules = [];

    while (!isAfter(date, en_end_date)) {
      intervals = [];
      for (let i = 0; i < rules.length; i++) {
        // Regras para dia específico
        if (rules[i].type === 'day') {
          const en_date = DateService.convertDateEn(rules[i].date);
          if (isEqual(date, en_date)) {
            intervals.push(rules[i].intervals);
          }
        }

        // Regra diariamente
        if (rules[i].type === 'daily') {
          intervals.push(rules[i].intervals);
        }

        // Regra semanalmente
        if (rules[i].type === 'weekly') {
          const weekday = getDay(date);
          if (rules[i].weekday.indexOf(weekday) > -1) {
            intervals.push(rules[i].intervals);
          }
        }
      }

      // Define intervalos por dia
      if (intervals.length > 0) {
        schedules.push({ day: DateService.convertDatePt(date), intervals });
      }
      // Próximo dia
      date = addDays(date, 1);
    }

    return res.send(JSON.stringify(schedules));
  }
}

export default new ScheduleController();
