import fs from 'fs';
import * as Yup from 'yup';
import { resolve } from 'path';

const filePath = resolve(__dirname, '..', 'rules.json');
const file = fs.readFileSync(filePath, 'utf-8');

class RuleController {
  async index(req, res) {
    const rules = JSON.parse(file);
    return res.send(rules);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      type: Yup.string().required(),
      date: Yup.string().when('type', (type, field) =>
        type === 'day' ? field.required() : field
      ),
      weekday: Yup.array().when('type', (type, field) =>
        type === 'weekly' ? field.required() : field
      ),
      intervals: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro de validação dos campos.' });
    }

    const { type, date, weekday, intervals } = req.body;
    const rules = JSON.parse(file);
    const lastId = rules.length > 0 ? rules[rules.length - 1].id : 0;

    // Regra para dia específico
    if (type === 'day') {
      const rule = {
        id: lastId + 1,
        type,
        date,
        weekday,
        intervals,
      };
      rules.push(rule);
      const rulesJson = JSON.stringify(rules);
      fs.writeFileSync(filePath, rulesJson);
    }

    // Regra diariamente
    if (type === 'daily') {
      const rule = {
        id: lastId + 1,
        type,
        date: '',
        weekday: '',
        intervals,
      };
      rules.push(rule);
      const rulesJson = JSON.stringify(rules);
      fs.writeFileSync(filePath, rulesJson);
    }

    // Regra semanalmente
    if (type === 'weekly') {
      const rule = {
        id: lastId + 1,
        type,
        date: '',
        weekday,
        intervals,
      };
      rules.push(rule);
      const rulesJson = JSON.stringify(rules);
      fs.writeFileSync(filePath, rulesJson);
    }

    return res.send({ message: 'Nova regra definida com sucesso.' });
  }

  async delete(req, res) {
    const id = parseInt(req.params.id, 10);
    const rules = JSON.parse(file);
    for (let i = 0; i < rules.length; i++) {
      if (rules[i].id === id) {
        rules.pop(rules[i]);
      }
    }
    const rulesJson = JSON.stringify(rules);
    fs.writeFileSync(filePath, rulesJson);

    return res.send({ message: 'Regra removida com sucesso.' });
  }
}

export default new RuleController();
