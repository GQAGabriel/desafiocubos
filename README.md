# Desafio Cubos Tecnologia

Api node.js para agendamento de horários na clínica

### Guia de instalação
- Clonar o repositório
- Na pasta do projeto rodar o comando yarn para baixar as dependências
- Rodar comando yarn dev para iniciar o servidor na porta 3333 (pré definida em src/server.js)

### Endpoints
https://documenter.getpostman.com/view/1350695/SWTHaufb?version=latest

### Objetivos
- [x] Cadastro de regras.
- [x] Listagem de regras.
- [x] Exclusão de regra.
- [x] Consultar horários.

### Estrutura de pastas e arquivos
- node_modules - Dependencias da api
- src - Core da api
    - app
        - controllers
            - ScheduleController.js - Endpoints para horários
            - RuleController.js - Endpoints para regras
        - services
            - DateService.js - Funções de apoio para manipular datas
        - **rules.json** - Arquivo que grava as regras
    - app.js - Arquivo que define o servidor
    - routes.js - Rotas da api
    - server.js - Arquivo que levanta o servidor
- editorconfig - Define padrão de escrita e espaçamento
- eslintrc.js - Definições de regras para o eslint
- prettierrc - Definições de regras para o prettier
- nodemon.json - Define chamada do sucrase para rodar com nodemon
- package.json - Gerencia as dependências

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

